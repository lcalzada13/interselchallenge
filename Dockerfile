FROM node:15.4.0-alpine3.12

#set working directory
WORKDIR /usr/src/app/

COPY . .
RUN npm install && npm run build

EXPOSE 3000
CMD ["npm", "start"]