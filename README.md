# Como ejecutar la app
El repositorio cuenta con un Dockerfile listo para construir la imagen y ejecutarla.

## Construir la imagen
Para construir la imagen usamos el siguiente comando:

`docker build -t 'nombreImagen' .`

## Ejecutamos la imagen
Para ejecutar la imagen usamos el siguiente comando:
` docker run -d -it -p 3000:3000 --name 'nombreContenedor' 'nombreImagen'`


