import './App.css';
import Navbar from './components/Navbar';
import Layout from './components/Layout';

const App = () => {
	return(
		<div id='app'>
			<Navbar />
			<Layout />
    	</div>
	);
}

export default App;
