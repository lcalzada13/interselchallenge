import '@testing-library/jest-dom';
import { screen, render } from '@testing-library/react';
import Branding from './Branding';

describe('<Branding/>', () => {

    beforeEach( () => {
        render (
            <Branding/>
        )
    });
    test('Renders company name', () => {
        screen.getByText('Health Overview');
    });
    test('Renders company logo', () => {
        const image = screen.getByAltText('Company logo');
        expect(image).toHaveAttribute('src','/img/logoTransparente_3.png');
    })
});
