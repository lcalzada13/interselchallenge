// Componente encargado de mostrar el logo y nombre de la empresa

import { FC } from 'react';

const Branding:FC = () => {
    return(
        <a href="/" className="branding">
            <img 
                src="/img/logoTransparente_3.png"
                alt="Company logo"
                width='50'
                height='50'
            />
            <h3>Health Overview</h3>
        </a>
    );
}

export default Branding;