// Componente encargado de crear cada cuadrado pequeño con las diferentes categorias 
// en las que puede entrar un porcentaje obtenido.

import { FC } from 'react';

interface IColorcode{
    colorClass: string;
    range: string;
    name: string;
    positionInScale: number;
}

const Colorcode:FC<IColorcode> = ({ colorClass, range, name, positionInScale }) => {
    let resultMarkerPosition = positionInScale;
    resultMarkerPosition = resultMarkerPosition > 35? 35: resultMarkerPosition;
    resultMarkerPosition = resultMarkerPosition < 0? 0: resultMarkerPosition;
    resultMarkerPosition = ((resultMarkerPosition * 90) / 35);
        
    return(
        <div className='colorSection' style={{left:resultMarkerPosition+'%'}}>
            <div className={`${colorClass} colorSquare`}>
            </div>
            <p className='colorScalePercentage'>{range}</p>
            <p className='colorScaleName'>{name}</p>
        </div>
    );
    
}

export default Colorcode;