import { useState } from 'react';
import '@testing-library/jest-dom';
import { screen, render } from '@testing-library/react';
import Form from './Form';
import userEvent from '@testing-library/user-event';

describe('<Form/>', () => {
    const user = userEvent.setup();
    const Wrapper = () => {
        const [showResult, setShowResult] = useState<Boolean>(false);
        const [finalResult, setFinalResult] = useState<number>(0);
        const [gender, setGender] = useState<string>('');
        return (
            <Form 
                setShowResult={setShowResult}
                setFinalResult={setFinalResult}
                setGender={setGender}
            />
        )
    };

    test('Renders form', () => {
        render(<Wrapper/>);
        screen.getByRole('radio', {name: 'Hombre'});
        screen.getByRole('radio', {name: 'Mujer'});
        screen.getByRole('spinbutton', {name: 'Altura (cm)'});
        screen.getByRole('spinbutton', {name: 'Peso (kg)'});
        screen.getByRole('spinbutton', {name: 'Cintura (cm)'});
        screen.getByRole('spinbutton', {name: 'Cuello (cm)'});
        expect(screen.getByRole('button', {name: 'Calcular'})).toBeDisabled();
        screen.getByRole('link', {name: 'Limpiar'});
    });

    test('When female option selected hip input is displayed and after that when male option is selected hip get removed', async () => {
        render(<Wrapper/>);
        await user.click(screen.getByRole('radio', {name: 'Mujer'}));
        screen.getByRole('spinbutton', {name: 'Cadera (cm)'});
        await user.click(screen.getByRole('radio', {name: 'Hombre'}));
        expect(screen.queryByRole('spinbutton', {name: 'Cadera (cm)'})).toBeNull();
    });

    describe('All male inputs are filled with data', () => {
        
        beforeEach( async () => {
            render(<Wrapper/>);
            await user.click(screen.getByRole('radio', {name: 'Hombre'}));
            await user.type(screen.getByRole('spinbutton', {name: 'Altura (cm)'}), '180');
            await user.type(screen.getByRole('spinbutton', {name: 'Peso (kg)'}), '80');
            await user.type(screen.getByRole('spinbutton', {name: 'Cintura (cm)'}), '90');
            await user.type(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}), '33');
        });

        test('Calculate button is not disabled and if removed input data calculate btn is disabled', async () => {
            const calculateBtn = screen.getByRole('button', {name: 'Calcular'});
            expect(calculateBtn).not.toBeDisabled();
            await user.clear(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}));
            expect(calculateBtn).toBeDisabled();
        });

        test('Clean form', async () => {
            await user.click(screen.getByRole('link', {name: 'Limpiar'}));
            // expect(screen.getByRole('spinbutton', {name: 'Hombre'})).toHaveValue(null);
            expect(screen.getByRole('spinbutton', {name: 'Altura (cm)'})).toHaveValue(null);
            expect(screen.getByRole('spinbutton', {name: 'Peso (kg)'})).toHaveValue(null);
            expect(screen.getByRole('spinbutton', {name: 'Cintura (cm)'})).toHaveValue(null);
            expect(screen.getByRole('spinbutton', {name: 'Cuello (cm)'})).toHaveValue(null);
        });

    });

    describe('All female inputs are filled with data', () => {
        beforeEach( async () => {
            render(<Wrapper/>);
            await user.click(screen.getByRole('radio', {name: 'Mujer'}));
            await user.type(screen.getByRole('spinbutton', {name: 'Altura (cm)'}), '180');
            await user.type(screen.getByRole('spinbutton', {name: 'Peso (kg)'}), '80');
            await user.type(screen.getByRole('spinbutton', {name: 'Cintura (cm)'}), '90');
            await user.type(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}), '33');
            await user.type(screen.getByRole('spinbutton', {name: 'Cadera (cm)'}),'69');
        });

        test('Calculate button is not disabled and if removed input data calculate btn is disabled', async () => {
            const calculateBtn = screen.getByRole('button', {name: 'Calcular'});
            expect(calculateBtn).not.toBeDisabled();
            await user.clear(screen.getByRole('spinbutton', {name: 'Cadera (cm)'}));
            expect(calculateBtn).toBeDisabled();
        });

    });

    
});
