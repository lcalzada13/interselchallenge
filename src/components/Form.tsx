// Componente de todo el formulario

import React, { FC, useEffect, useState } from 'react';
import Input from './Input';
import Radio from './Radio';
import inputsList from '../constants/inputsList';
import radiosList from '../constants/radiosList';
import womenExtraInput from '../constants/womenExtraInput';
import IForm from '../interfaces/IForm';
import IFormDataSet from '../interfaces/IFormDataSet';
import bodyFatPercentage from '../functions/bodyFatPercentage';
import checkDataIsValid from '../functions/checkDataIsValid';


const Form:FC<IForm> = ({ setShowResult,setFinalResult, setGender }) => {
    const [formDataSet, setFormDataSet] = useState<IFormDataSet>({gender:'',height: '', weight: '', waist: '', neck: '', hip: ''});
    const [disabledCalculate, setDisabledCalculate] = useState(true);
    const [inputsListState, setInputsListState] = useState(inputsList);
    
    useEffect( () => {
        if(formDataSet['gender'] === 'female'){
            let obj = inputsListState.find(x => x.id === 'hip');
            if(!obj){
                setInputsListState(inputsListState.concat(womenExtraInput));
            }
        }
        else{
            let obj = inputsListState.find(x => x.id === 'hip');
            if(obj){
                inputsListState.pop();
                setInputsListState([...inputsListState]);
            }
        }
        let amountEmpty = Object.values(formDataSet).filter(x => x === '').length;
        let amountZeros = Object.values(formDataSet).filter(x => x === 0).length;
        let sumAmounts = amountEmpty + amountZeros;
        if((amountEmpty === 0 && amountZeros === 0)){
            setDisabledCalculate(false);
        }
        else if((sumAmounts === 1)){
            if(formDataSet['gender'] === 'male' && ['',0].includes(formDataSet['hip'])){
                setDisabledCalculate(false);
            }
            else{
                setDisabledCalculate(true);
            }
        }
        else{
            setDisabledCalculate(true);
        }
    },[formDataSet, inputsListState]);
    const onInputChange = (x: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = x.target;
        setFormDataSet( prevState => ({
            ...prevState,
            [name]: isNaN(+value) ? value: +value
        })
        );
    }
    const cleanInputs = () => {
        setFormDataSet({gender:'',height: '', weight: '', waist: '', neck: '', hip: ''});
        setShowResult(false);
    };
    
    const calculateBFP = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        let result:number;
        if(checkDataIsValid(formDataSet)){
            result = bodyFatPercentage(formDataSet);
        }
        else{
            result = 0;
        }
        setShowResult(true);
        setFinalResult(result);
        setGender(formDataSet.gender);
    };
    return(
        <form onSubmit={calculateBFP}>
            <h5 className="titleRadios">Género</h5>
            <div className="rowRadios">
                {radiosList.map( ({ id,value, ...rest}) => 
                    <Radio 
                        key={id}
                        id={id}
                        value={value}
                        checked={formDataSet['gender'] === value? true: false}
                        {...rest}
                        onChange={onInputChange}
                    />
                )}
            </div>
            {inputsListState.map( ({ id, ...rest }) => 
                <Input 
                    key={id}
                    id={id}
                    value={formDataSet[id as keyof typeof formDataSet]} 
                    {...rest}
                    onChange={onInputChange}
                />
            )}
            <button  className={`highlightedBtn btnStyle ${disabledCalculate ? "greyOut":""}`} disabled={disabledCalculate}>Calcular</button>
            <a   className="btnStyle cleanLink" onClick={cleanInputs}>Limpiar</a>
        </form>
    );        
};

export default Form;