// Componente de input para el formulario. Sin contar los inputs tipo radio que estan 
// en un componente diferente
import { FC } from 'react';
import InputProps from '../interfaces/InputProps';


const Input:FC<InputProps> = ({ label,htmlFor, ...rest}) => {
    return(
        <div className="rowForm">
            <label htmlFor={htmlFor}>{label}</label>
            <input
                min={'0'} 
                {...rest}
            />
        </div>
    );
}

export default Input;