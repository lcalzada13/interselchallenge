import '@testing-library/jest-dom';
import { screen, render } from '@testing-library/react';
import Layout from './Layout';
import userEvent from '@testing-library/user-event';

describe('<Layout/>', () => {

    describe( 'Basic elements are displayed', () => {
        beforeEach( () => {
            render (
                <Layout/>
            )
        });

        test('Renders title calculator', () => {
            screen.getByText("Calculadora de Grasa Corporal");
        });

        test('Renders subtitle calculator', () => {
            screen.getByTestId("subtitleCalculator");
        });

    });

    describe('All male inputs are filled with data', () => {
        const user = userEvent.setup();
        beforeEach( async () => {
            render(<Layout/>);
            await user.click(screen.getByRole('radio', {name: 'Hombre'}));
            await user.type(screen.getByRole('spinbutton', {name: 'Altura (cm)'}), '180');
            await user.type(screen.getByRole('spinbutton', {name: 'Peso (kg)'}), '80');
            await user.type(screen.getByRole('spinbutton', {name: 'Cintura (cm)'}), '90');
            await user.type(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}), '33');
        });

        test('Display result', async () => {
            await user.click(screen.getByRole('button', {name: 'Calcular'}));
            screen.getByText('2-5%');
            screen.getByText('6-13%');
            screen.getByText('14-17%');
            screen.getByText('18-24%');
            screen.getByText('25% +');
        });

        test('Test get result, expected 23.2', async () => {
            await user.click(screen.getByRole('button', {name: 'Calcular'}));
            screen.getByText('23.23%');
        });
        
    });

    test('Test get result with negative values, expected error', async () => {
        const user = userEvent.setup();
        render(<Layout/>);
        await user.click(screen.getByRole('radio', {name: 'Hombre'}));
        await user.type(screen.getByRole('spinbutton', {name: 'Altura (cm)'}), '-180');
        await user.type(screen.getByRole('spinbutton', {name: 'Peso (kg)'}), '80');
        await user.type(screen.getByRole('spinbutton', {name: 'Cintura (cm)'}), '90');
        await user.type(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}), '33');
        await user.click(screen.getByRole('button', {name: 'Calcular'}));
        screen.getByText('Error');
    });

    test('Test get result with string values, expected error', async () => {
        const user = userEvent.setup();
        render(<Layout/>);
        await user.click(screen.getByRole('radio', {name: 'Hombre'}));
        await user.type(screen.getByRole('spinbutton', {name: 'Altura (cm)'}), '180');
        await user.type(screen.getByRole('spinbutton', {name: 'Peso (kg)'}), 'wrong');
        await user.type(screen.getByRole('spinbutton', {name: 'Cintura (cm)'}), '90');
        await user.type(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}), '33');
        await user.click(screen.getByRole('button', {name: 'Calcular'}));
        expect(screen.queryByText('Tu resultado')).toBeNull();
    });

    describe('All female inputs are filled with data', () => {
        const user = userEvent.setup();
        beforeEach( async () => {
            render(<Layout/>);
            await user.click(screen.getByRole('radio', {name: 'Mujer'}));
            await user.type(screen.getByRole('spinbutton', {name: 'Altura (cm)'}), '180');
            await user.type(screen.getByRole('spinbutton', {name: 'Peso (kg)'}), '80');
            await user.type(screen.getByRole('spinbutton', {name: 'Cintura (cm)'}), '90');
            await user.type(screen.getByRole('spinbutton', {name: 'Cuello (cm)'}), '33');
            await user.type(screen.getByRole('spinbutton', {name: 'Cadera (cm)'}), '33');
        });

        test('Display result', async () => {
            await user.click(screen.getByRole('button', {name: 'Calcular'}));
            screen.getByText('10-13%');
            screen.getByText('14-20%');
            screen.getByText('21-24%');
            screen.getByText('25-31%');
            screen.getByText('32% +');
        });
        
    });
});
