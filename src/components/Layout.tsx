// Componente encargado de mostrar el título e instrucciones de la calculadora
// además llama al formulario y al componente del resultado.

import { FC, useState} from 'react';
import Form from './Form';
import Result from './Result';

const Layout:FC = () => {
    const [showResult, setShowResult] = useState<Boolean>(false);
    const [finalResult, setFinalResult] = useState<number>(0);
    const [gender, setGender] = useState<string>('');

    return(
        <div className="Layout">
            <div className="leftSide">
                <div className='infoCalculator'>
                    <h1>Calculadora de Grasa Corporal</h1>
                    <p data-testid="subtitleCalculator">El método de la Marina de Estados Unidos (US Navy Method) ofrece una manera 
                        sencilla de calcular un aproximado
                        del porcentaje de tejido adiposo en el cuerpo de una persona.
                    </p>
                    <p>Los valores requeridos por la fórmula son los siguientes:</p>
                </div>
                <Form 
                    setShowResult={setShowResult}
                    setFinalResult={setFinalResult}
                    setGender={setGender}
                />
            </div>
            <div className="rightSide">
                {showResult ? 
                    <Result 
                        finalResult={finalResult}
                        gender={gender}
                    />:
                    null
                }
            </div>
        </div>
    );
}

export default Layout;