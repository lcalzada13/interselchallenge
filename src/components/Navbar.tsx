// Componente para toda la barra superior. 
// Realmente aquí solo mandamos a llamar componentes de branding y de topMenu

import Branding from './Branding';
import TopMenu from './TopMenu';
import { FC } from 'react';

const Navbar:FC = () => {
    return(
        <div className="navbar">
            <Branding />
            <TopMenu />
        </div>
    );
}

export default Navbar;