// Componente para los inputs de genero tipo radio

import { FC } from "react";

interface IRadio{
    id: string;
    name: string;
    label: string;
    value: string;
    onChange: React.ChangeEventHandler<HTMLInputElement>;
    checked: boolean;
}

const Radio:FC<IRadio> = ({ id, label, checked, ...rest }) => {
    
    return(
        <div className="radiosContainer">
            <input 
                type="radio" 
                id={id}
                checked={checked}
                {...rest} />
            <label htmlFor={id}>{label}</label>
        </div>
    );
};

export default Radio;