// Componente para mostrar los resultados. 
// tanto el texto con el valor como la escala de colores con la categoria

import { FC, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortDown } from '@fortawesome/free-solid-svg-icons';
import Colorcode from './Colorcode';
import IFinalResult from '../interfaces/IFinalResult';
import percentagesFemales from '../constants/percentagesFemales';
import percentagesMales from '../constants/percentagesMales';

const Result:FC<IFinalResult> = ({ finalResult, gender }) => {
    const [markerPosition, setMarkerPosition] = useState<string | number>('1%');
    const selectedPercentages = gender === 'female'? percentagesFemales: percentagesMales;

    useEffect(() => {
        let resultMarkerPosition = finalResult;
        resultMarkerPosition = resultMarkerPosition > 35? 35: resultMarkerPosition;
        resultMarkerPosition = resultMarkerPosition < 0? 0: resultMarkerPosition;
        resultMarkerPosition = ((resultMarkerPosition * 90) / 35);
        setMarkerPosition(resultMarkerPosition+'%');
    },[finalResult]);
    
    return (
        <div className='resultContainer'>
            <h2>Tu resultado: {finalResult > 0? finalResult+'%': 'Verifique los datos'}</h2>
            <div className="resultColorScale">
                <div className='resultMarker'  style={{marginLeft:markerPosition}}>
                    <p >{finalResult > 0? finalResult+'%':'Error'}</p>
                    <FontAwesomeIcon icon={faSortDown} title='Tu resultado' />    
                </div>
            </div>
            <div className='colorScaleDescription'>
                {selectedPercentages.map( ({colorClass, ...rest}) => 
                    <Colorcode 
                        key={colorClass}
                        colorClass={colorClass}
                        {...rest}
                    />
                )}
            </div>
        </div>
    );
}

export default Result;