import '@testing-library/jest-dom';
import { screen, render } from '@testing-library/react';
import TopMenu from './TopMenu';

describe('<TopMenu/>', () => {

    beforeEach( () => {
        render (
            <TopMenu/>
        )
    });
    test('Renders display menu btn', () => {
        screen.getByTitle('Menu');
    });
});
