// Componente del menu superior.
// Aunque realmente solo es un icono ya que no tenemos opciones en el menu

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FC } from 'react';

const TopMenu:FC = () => {
    return(
        <div id='menuBtn'>
            <FontAwesomeIcon icon={faBars} title='Menu' />
        </div>
    );
}

export default TopMenu;