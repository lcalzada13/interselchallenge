// Constante con los diferentes inputs base que independiente del genero son necesarios
// Si se desea agregar un nuevo dato base, solo es necesario agregar una nueva linea al arreglo

const inputsList = [
    {label:"Altura (cm)", htmlFor:"height", type:"number", placeholder:"Escribe tu altura", name:"height" , id:"height"},
    {label:"Peso (kg)", htmlFor:"weight", type:"number", placeholder:"Escribe tu peso", name:"weight" , id:"weight"},
    {label:"Cintura (cm)", htmlFor:"waist", type:"number", placeholder:"Medida de tu cintura", name:"waist" , id:"waist"},
    {label:"Cuello (cm)", htmlFor:"neck", type:"number", placeholder:"Medida de tu cuello", name:"neck" , id:"neck"}
    
];

export default inputsList;