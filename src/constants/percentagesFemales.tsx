// Constantes de las diferentes categorías de los porcentajes para mujeres
// Tambien aquí podemos controlar como se distribuyen los cuadrados pequeños de colores

const percentagesFemales = [
    {
        range:'10-13%',
        positionInScale:2.5, 
        colorClass:'blueSquare',
        name:'Esencial'
    },
    {
        range:'14-20%',
        positionInScale:9.5,
        colorClass:'greenSquare',
        name:'Deportista'
    },
    {
        range:'21-24%',
        positionInScale:15.5,
        colorClass:'limeSquare',
        name:'Fitness'
    },
    {
        range:'25-31%',
        positionInScale:21,
        colorClass:'yellowSquare',
        name:'Aceptable'
    },
    {
        range:'32% +',
        positionInScale:25.5,
        colorClass:'orangeSquare',
         name:'Obeso'
    }
];

export default percentagesFemales;