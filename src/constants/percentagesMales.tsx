// Constantes de las diferentes categorías de los porcentajes para hombres
// Tambien aquí podemos controlar como se distribuyen los cuadrados pequeños de colores


const percentagesMales = [
    {
        range:'2-5%',
        positionInScale:2.5, 
        colorClass:'blueSquare',
        name:'Esencial'
    },
    {
        range:'6-13%',
        positionInScale:9.5,
        colorClass:'greenSquare',
        name:'Deportista'
    },
    {
        range:'14-17%',
        positionInScale:15.5,
        colorClass:'limeSquare',
        name:'Fitness'
    },
    {
        range:'18-24%',
        positionInScale:21,
        colorClass:'yellowSquare',
        name:'Aceptable'
    },
    {
        range:'25% +',
        positionInScale:25.5,
        colorClass:'orangeSquare', 
        name:'Obeso'
    }
];


export default percentagesMales;



