// Constante con el listado de los diferentes generos que se manejan.


const radiosList = [
    {id:"genderH", name:"gender", label:"Hombre", value:"male"},
    {id:"genderM", name:"gender", label:"Mujer", value:"female"}
];
export default radiosList;