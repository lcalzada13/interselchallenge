// Constante con los diferentes inputs que se agregan si se elige la opcion de genero femenino
// Si se desea agregar un nuevo dato extra, solo es necesario agregar una nueva linea al arreglo

const womenExtraInput = {label:"Cadera (cm)", htmlFor:"hip", type:"number", placeholder:"Medida de tu cadera", name:"hip" , id:"hip"};

export default womenExtraInput;