// Funcion para el calculo del porcentaje de masa corporal

import IFormDataSet from '../interfaces/IFormDataSet';

const bodyFatPercentage = (formDataSet:IFormDataSet) => {
    let coefficientWaistNeck:number, coefficientHeight:number, denominator:number = 0;
    if(formDataSet.gender === 'female'){
        coefficientWaistNeck = Math.log10(Number(formDataSet.waist) + Number(formDataSet.hip) - Number(formDataSet.neck));
        coefficientHeight= Math.log10(Number(formDataSet.height));
        denominator = (1.29579 - (0.35004 * coefficientWaistNeck) + (0.22100 * coefficientHeight));
    }
    else{
        coefficientWaistNeck = Math.log10(Number(formDataSet.waist) - Number(formDataSet.neck));
        coefficientHeight= Math.log10(Number(formDataSet.height));
        denominator = (1.0324 - (0.19077 * coefficientWaistNeck) + (0.15456 * coefficientHeight));
    }        
    return +(((495/ denominator)-450)).toFixed(2);
}

export default bodyFatPercentage;