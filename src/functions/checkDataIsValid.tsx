// Funcion para validar si los inputs tienen valores permitidos.
// Se puede hacer más robusta la validación, mientras la funcion regrese un true o false el resto
// del codigo lo maneja

import IFormDataSet from '../interfaces/IFormDataSet';

const checkDataIsValid = (formDataSet:IFormDataSet) => {
    return Object.values(formDataSet).some(x => x > 0 );
}

export default checkDataIsValid;