import { Dispatch, SetStateAction } from 'react';

interface IForm{
    setShowResult: Dispatch<SetStateAction<Boolean>>;
    setFinalResult: Dispatch<SetStateAction<number>>;
    setGender: Dispatch<SetStateAction<string>>;
};

export default IForm;