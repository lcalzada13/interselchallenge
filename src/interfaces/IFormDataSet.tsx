interface IFormDataSet{
    gender: string;
    height: number | string;
    weight: number  | string;
    waist: number |string;
    neck: number | string;
    hip: number | string;
}

export default IFormDataSet