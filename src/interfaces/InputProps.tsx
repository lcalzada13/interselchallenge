interface InputProps {
    label: string;
    htmlFor: string;
    type: string;
    placeholder: string;
    name: string;
    id: string;
    value: number|string;  
    onChange: React.ChangeEventHandler<HTMLInputElement>;
};

export default InputProps;